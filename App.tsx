/**
 * Tabesoku
 * https://gitlab.com/colourdelete/tabesoku
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import * as firebase from 'firebase';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '@material-ui/icons/Home';
import { Icon as MdiIcon } from '@mdi/react'
import { mdiCharity } from '@mdi/js';
import { Icon as RNEIcon } from 'react-native-elements';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {Button} from "react-native-paper";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyBkD6apwICvS5YxdRV4zwvssbtmvJrOx3s",
    authDomain: "tabesoku-a6014.firebaseapp.com",
    databaseURL: "https://tabesoku-a6014.firebaseio.com",
    storageBucket: "tabesoku-a6014.appspot.com",
  };
const firebaseApp = firebase.initializeApp(firebaseConfig);

function HomeScreen({ navigation }) {
  return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
          {/*<Home />*/}
        <Button
            onPress={() => navigation.push('Details')}
        >
          <Text>Go to Details</Text>
        </Button>
        <Button
            onPress={() => navigation.navigate('Home')}
        >
          <Text>Go to Home</Text>
        </Button>
        <Button
            onPress={() => navigation.goBack()}
        >
          <Text>Go back</Text>
        </Button>
      </View>
  );
}

function ListScreen({ navigation }) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings!</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.push('Details')}
            >
                <Text>Go to Details</Text>
            </Button>
            <Button
                title="Go to Home"
                onPress={() => navigation.navigate('Home')}
            >
                <Text>Go to Home</Text>
            </Button>
            <Button
                title="Go back"
                onPress={() => navigation.goBack()}
            >
                <Text>Go back</Text>
            </Button>
        </View>
    );
}

function RecipesScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Details Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.push('Details')}
            >
                <Text>Go to Details</Text>
            </Button>
            <Button
                title="Go to Home"
                onPress={() => navigation.navigate('Home')}
            >
                <Text>Go to Home</Text>
            </Button>
            <Button
                title="Go back"
                onPress={() => navigation.goBack()}
            >
                <Text>Go back</Text>
            </Button>
        </View>
    );
}

function CharityScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Details Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.push('Details')}
            >
                <Text>Go to Details</Text>
            </Button>
            <Button
                title="Go to Home"
                onPress={() => navigation.navigate('Home')}
            >
                <Text>Go to Home</Text>
            </Button>
            <Button
                title="Go back"
                onPress={() => navigation.goBack()}
            >
                <Text>Go back</Text>
            </Button>
        </View>
    );
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function App() {
  return (
      <NavigationContainer>
          <Tab.Navigator
              screenOptions={({ route }) => ({
                  tabBarIcon: ({ focused, color, size }) => {
                      let iconName, type, backend, path;
                      backend = 'rne'

                      if (route.name === 'Home') {
                          iconName = 'home'
                          type = 'material-community'
                      } else if (route.name === 'Recipes') {
                          iconName = 'book-open'
                          type = 'material-community'
                      } else if (route.name === 'Charity') {
                          iconName = 'hand'
                          type = 'material-community'
                          path = mdiCharity
                          backend = 'mdi'
                      } else if (route.name === 'List') {
                          iconName = 'playlist-check'
                          type = 'material-community'
                      }

                      // You can return any component that you like here!
                      if (backend === 'rne') return <RNEIcon name={iconName} size={size} color={color} type={type} />
                      else if (backend === 'mdi') return <MdiIcon path={path} />//size={size} color={color} />
                  },
              })}
              tabBarOptions={{
                  activeTintColor: 'tomato',
                  inactiveTintColor: 'gray',
              }}
          >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{ title: 'Home' }}
            />
            <Stack.Screen
                name="Recipes"
                component={RecipesScreen}
                options={{ title: 'Recipes' }}
            />
            <Stack.Screen
                name="harity"
                component={CharityScreen}
                options={{ title: 'Charity' }}
            />
            <Stack.Screen
                name="List"
                component={ListScreen}
                options={{ title: 'List' }}
            />
        </Tab.Navigator>
      </NavigationContainer>
  );
}

// const App: () => React$Node = () => {
//   return (
//     <>
//       <NavigationContainer>
//         <StatusBar barStyle="dark-content" />
//         <SafeAreaView>
//           <ScrollView
//               contentInsetAdjustmentBehavior="automatic"
//               style={styles.scrollView}>
//             <Header />
//             {global.HermesInternal == null ? null : (
//                 <View style={styles.engine}>
//                   <Text style={styles.footer}>Engine: Hermes</Text>
//                 </View>
//             )}
//             <View style={styles.body}>
//               <View style={styles.sectionContainer}>
//                 <Text style={styles.sectionTitle}>Step One</Text>
//                 <Text style={styles.sectionDescription}>
//                   Edit <Text style={styles.highlight}>App.js</Text> to change this
//                   screen and then come back to see your edits.
//                 </Text>
//               </View>
//               <View style={styles.sectionContainer}>
//                 <Text style={styles.sectionTitle}>See Your Changes</Text>
//                 <Text style={styles.sectionDescription}>
//                   <ReloadInstructions />
//                 </Text>
//               </View>
//               <View style={styles.sectionContainer}>
//                 <Text style={styles.sectionTitle}>Debug</Text>
//                 <Text style={styles.sectionDescription}>
//                   <DebugInstructions />
//                 </Text>
//               </View>
//               <View style={styles.sectionContainer}>
//                 <Text style={styles.sectionTitle}>Learn More</Text>
//                 <Text style={styles.sectionDescription}>
//                   Read the docs to discover what to do next:
//                 </Text>
//               </View>
//               <LearnMoreLinks />
//             </View>
//           </ScrollView>
//         </SafeAreaView>
//       </NavigationContainer>
//     </>
//   );
// };
//
// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },
//   engine: {
//     position: 'absolute',
//     right: 0,
//   },
//   body: {
//     backgroundColor: Colors.white,
//   },
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//     color: Colors.black,
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     color: Colors.dark,
//   },
//   highlight: {
//     fontWeight: '700',
//   },
//   footer: {
//     color: Colors.dark,
//     fontSize: 12,
//     fontWeight: '600',
//     padding: 4,
//     paddingRight: 12,
//     textAlign: 'right',
//   },
// });
//
// class HomeScreen extends React.Component {
//   render() {
//     return(
//         <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//           <Text> This is my Home screen </Text>
//         </View>
//     );
//   }
// }
//
// class ExploreScreen extends React.Component {
//   render() {
//     return(
//         <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//           <Text> This is my Explore screen </Text>
//         </View>
//     );
//   }
// }
//
// const Stack = createStackNavigator();
//
// function RootStack() {
//   return (
//       <Stack.Navigator
//           initialRouteName="Home"
//           screenOptions={{ gestureEnabled: false }}
//       >
//         <Stack.Screen
//             name="Home"
//             component={HomeScreen}
//             options={{ title: 'My app' }}
//         />
//         <Stack.Screen
//             name="Profile"
//             component={ProfileScreen}
//             initialParams={{ user: 'me' }}
//         />
//       </Stack.Navigator>
//   );
// }

// const AppContainer = createAppContainer(bottomTabNavigator);

export default App;
